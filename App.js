/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
 import React from 'react';
 import { StatusBar, View, Text} from 'react-native';
 import { HomeContainer} from './app/lib/navigationRoutes';

 Text.defaultProps = Text.defaultProps || {};
 Text.defaultProps.allowFontScaling = false;
 
 const App = () => {
 
   return (
     <>
       <StatusBar
         barStyle="dark-content"
         translucent={true}
         hidden={false}
         backgroundColor={'transparent'}
       />
 
       <View style={{width: '100%', height: '100%'}}>
         <HomeContainer />
       </View>
     </>
   );
 };
 
 export default App;
 
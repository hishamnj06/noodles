import React, { useState } from 'react';
import {
  Text,
  View,
  FlatList
} from 'react-native';
import { styles } from './styles';

import FastImage from '../FastImage/FastImage'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';

const HomeView = props => {
  const {
    data,
    images,
    gotoRestaurant,
    dataAll,
    setData
  } = props;

  
  let [ascDesc, setAscDesc] = useState('');

  const searchTxt = (txt) => {
    let filtered = data.filter(item => { return item.Variety.indexOf(txt) != -1 });
    setData(filtered);
  }

  const sort = (type) => {
    setAscDesc(type);

    let sorted = [];
    if(type == 'asc') {
      sorted = data.sort((a,b) => (a.Stars > b.Stars) ? 1 : ((b.Stars > a.Stars) ? -1 : 0));
    } else {
      sorted = data.sort((a,b) => (a.Stars > b.Stars) ? -1 : ((b.Stars > a.Stars) ? 1 : 0));
    }
    setData(sorted);

  }

  return (
    
    <View style={[styles.screenMain]}>

       <View style={[styles.noodle, {height: 20}]}>
        <Text style={styles.veriety}>List of items</Text>
        </View>
       <View style={[styles.noodle, {height: 80}]}>
        <View>
          <TextInput
            style={styles.search}
            placeholder={'Search'}
            onChangeText={(txt) => searchTxt(txt) }
          />
          <View style={styles.controls}>
            <Text style={{alignSelf: 'center'}}>Sort by Stars</Text>
            <TouchableOpacity 
              onPress={() => sort('asc') }
              style={[styles.ascdesc, ascDesc == 'asc' && {backgroundColor: 'lightblue'}]}>
              <Text>ASC</Text>
            </TouchableOpacity>
            <TouchableOpacity 
              onPress={() => sort('desc') }
              style={[styles.ascdesc, ascDesc == 'desc' && {backgroundColor: 'lightblue'}]}>
              <Text>DESC</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={dataAll}
          extraData={dataAll}
          renderItem={({item, index}) => {
            const rnd = index % images.length;
            const randomImage = images?.[rnd]?.Image || 'https://s3-ap-southeast-1.amazonaws.com/he-public-data/indexee3e8a8.jpeg';
            return (
              <TouchableOpacity onPress={() => gotoRestaurant(item, randomImage)}>
                <View style={styles.noodle}>

                  <FastImage
                    resizeMode="contain"
                    photoURL={randomImage}
                    extraStyle={styles.maggie}
                    style={styles.maggie}
                  />
                    <Text style={[styles.veriety, {marginTop: 10} ]}>Veriety: {item.Variety}</Text>
                    <Text style={styles.veriety}>Stars: {item.Stars}</Text>
                    <Text style={styles.veriety}>Brand: {item.Brand}</Text>
                </View>
              </TouchableOpacity>
            )
          }}
        />
    </View>
  );
};

export default HomeView;

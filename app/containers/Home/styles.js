import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    backgroundColor: 'lightblue',
    paddingTop: 50
  },
  formWrapper: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius:30,
    borderTopRightRadius:30,
  },
  noodle: {borderColor: '#123', borderWidth: 1, borderRadius: 15, margin: 10, backgroundColor: '#fff'},
  veriety: {alignSelf: 'center', color: 'red'},
  maggie: {width: 200, height: 200, alignSelf: 'center', marginTop: 5},
  search: {height: 40, width: '90%', borderColor: 'gray', borderWidth: 1, alignSelf: 'center', marginTop: 5},
  controls: {flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10},
  ascdesc: {borderColor: 'gray', borderWidth: 1 }
});

export { styles };

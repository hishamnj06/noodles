import React, {useEffect, useState} from 'react';
import HomeView from './HomeView';

import {
  getHomeContent,
  getHomeImgContent
} from '../../actions';

import {useDispatch, useSelector} from 'react-redux';

const HomeScreen = props => {
  const dispatch = useDispatch();

  const { data, img_data } = useSelector(state => state.home);
  let [dataAll, setData] = useState([]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    dispatch(getHomeImgContent());
    dispatch(getHomeContent());
  };

  //did update
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if(data && dataAll.length == 0) {
      setData(data)
    }
  };

  const gotoRestaurant = (data, image) => {
    props.navigation.navigate('Restaurant', {data: data, image});
  }

  return (
    <HomeView
      dataAll={dataAll}
      setData={setData}
      data={data} images={img_data} gotoRestaurant={gotoRestaurant} 
    />
  );
};

export default HomeScreen;

import React, {useEffect} from 'react';
import RestaurantView from './RestaurantView';

const Restaurant = props => {

  const data = props.route.params.data;
  const image = props.route.params.image;

  return (
    <RestaurantView data={data} image={image} goBack={() => props.navigation.goBack()}/>
  );
};

export default Restaurant;

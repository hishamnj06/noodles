import React, { useState } from 'react';
import {
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import { styles } from './styles';
import FastImage from '../FastImage/FastImage';

const RestaurantView = props => {
  const {
    data,
    image,
    goBack
  } = props;

  return (
    
    <View style={[styles.screenMain]}>

       <View style={[styles.noodle, {height: 20}]}>
        <Text style={styles.veriety}>Restaurant Details</Text>
        </View>

        <View style={styles.noodle}>
          <FastImage
            resizeMode="contain"
            photoURL={image}
            extraStyle={styles.maggie}
            style={styles.maggie}
          />
            <Text style={[styles.veriety, {marginTop: 10} ]}>Veriety: {data.Variety}</Text>
            <Text style={styles.veriety}>Stars: {data.Stars}</Text>
            <Text style={styles.veriety}>Brand: {data.Brand}</Text>
          </View>

          <View style={[styles.noodle, {height: 20}]}>
            <TouchableOpacity onPress={() => goBack()}>
           <Text style={styles.back}>back to home</Text>
           </TouchableOpacity>
          </View>

    </View>
  );
};

export default RestaurantView;

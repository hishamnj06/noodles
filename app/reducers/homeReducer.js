import * as ActionTypes from '../actions/types';

const defaultState = {
  isLoading: false,
  img_data: [],
  data: []
};

export default function homeReducer(state = defaultState, action) {
  switch (action.type) {
    case ActionTypes.GET_HOME_CONTENT_SUCCESS:
      return Object.assign({}, state, {
        isLoading: true,
        data: action.payload,
      });

    case ActionTypes.GET_HOME_IMG_CONTENT_SUCCESS:
      return Object.assign({}, state, {
        isLoading: true,
        img_data: action.payload,
      });

    default:
      return state;
  }
}

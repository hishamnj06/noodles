import {GET_HOME_CONTENT, GET_HOME_IMG_CONTENT} from './types';
import {executeApiAction} from '../lib/apiHelper';

export const getHomeContent = () => {
  const home_api = 'https://s3-ap-southeast-1.amazonaws.com/he-public-data/TopRamen8d30951.json';
  return async dispatch => {
    dispatch(
      executeApiAction('get', home_api, GET_HOME_CONTENT, false),
    );
  };
};

export const getHomeImgContent = () => {
  const home_api = 'https://s3-ap-southeast-1.amazonaws.com/he-public-data/noodlesec253ad.json';
  return async dispatch => {
    dispatch(
      executeApiAction('get', home_api, GET_HOME_IMG_CONTENT, false),
    );
  };
};
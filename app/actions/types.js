export const SUCCESS_SUFFIX = '_SUCCESS';
export const ERROR_SUFFIX = '_ERROR';

export const GET_HOME_CONTENT = 'home_content';
export const GET_HOME_CONTENT_SUCCESS = 'home_content' + SUCCESS_SUFFIX;
export const GET_HOME_CONTENT_ERROR = 'home_content' + ERROR_SUFFIX;

export const GET_HOME_IMG_CONTENT = 'home_images';
export const GET_HOME_IMG_CONTENT_SUCCESS = 'home_images' + SUCCESS_SUFFIX;
export const GET_HOME_IMG_CONTENT_ERROR = 'home_images' + ERROR_SUFFIX;
import {ERROR_SUFFIX, SUCCESS_SUFFIX} from '../actions/types';
import callApi from './callApi';

const apiHelper = {
  getBaseUrl: () => {
    return '';
  },
};

export default apiHelper;

export const executeApiAction = (
  method,
  api,
  action,
  authorize = false,
  data = {},
  onSuccess = () => {},
  onError = () => {},
) => {
  const actionSuccess = action + SUCCESS_SUFFIX;
  const actionError = action + ERROR_SUFFIX;

  return async (dispatch, getState) => {
    try {
      dispatch({type: action});

      const authorizationToken = '';

      const response = await callApi(method, api, authorizationToken, data);
      if (response.status !== 200) {
        throw new Error();
      }

      dispatch({type: actionSuccess, payload: response.data});
      onSuccess(response.data);
    } catch (error) {
      if (error.response) {
        dispatch({
          type: actionError,
          payload: error.response,
        });

        onError(error.response);
      }
    }
  };
};

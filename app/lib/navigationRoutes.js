import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../containers/Home';
import Restaurant from '../containers/Restaurant';

const MainScreenStack = createStackNavigator();
const MainScreenStackNavigator = props => {
  return (
    <MainScreenStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>

      <MainScreenStack.Screen name="Home" component={HomeScreen} />
      <MainScreenStack.Screen name="Restaurant" component={Restaurant} />

    </MainScreenStack.Navigator>
  );
};

export const HomeContainer = () => {
  return (
    <NavigationContainer>
      <MainScreenStackNavigator/>
    </NavigationContainer>
  );
};

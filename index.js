/**
 * @format
 */

import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/es/integration/react';
import {store, persistor} from './configureStore';

export default class Noodles extends Component {
    render() {
      return (
        <Provider store={store}>
          <PersistGate persistor={persistor} />
          <App />
        </Provider>
      );
    }
  }

AppRegistry.registerComponent(appName, () => Noodles);
